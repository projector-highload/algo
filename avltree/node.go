package avltree

import (
	"github.com/m1gwings/treedrawer/tree"
)

type Node struct {
	Key    int
	Value  interface{}
	Parent *Node
	Left   *Node
	Right  *Node
	Height uint8
}

func (n *Node) find(key int) *Node {
	if n == nil {
		return nil
	}

	if n.Key == key {
		return n
	}

	if n.Key > key {
		return n.Left.find(key)
	}

	return n.Right.find(key)
}

func (n *Node) findMin() *Node {
	if n.Left == nil {
		return n
	}

	return n.Left.findMin()
}

func (n *Node) removeMin() *Node {
	if n.Left == nil {
		return n.Right
	}

	n.Left = n.Left.removeMin()

	return n.balance()
}

func (n *Node) remove(key int) *Node {
	if n == nil {
		return nil
	}

	if n.Key < key {
		n.Right = n.Right.remove(key)
		return n.balance()
	}

	if n.Key > key {
		n.Left = n.Left.remove(key)
		return n.balance()
	}

	l := n.Left
	r := n.Right

	if r == nil {
		return l
	}

	min := r.findMin()
	min.Right = r.removeMin()
	min.Left = l

	return min.balance()
}

func (n *Node) insert(newNode *Node) *Node {
	if n.Key < newNode.Key {
		if n.Right == nil {
			n.Right = newNode
		} else {
			n.Right = n.Right.insert(newNode)
		}
		n.Right.Parent = n
	} else {
		if n.Left == nil {
			n.Left = newNode
		} else {
			n.Left = n.Left.insert(newNode)
		}
		n.Left.Parent = n
	}

	return n.balance().balance()
}

func (n *Node) bFactor() int8 {
	return int8(n.Right.height()) - int8(n.Left.height())
}

func (n *Node) height() uint8 {
	if n == nil {
		return 0
	}

	return n.Height
}

func (n *Node) fixHeight() {
	lh := n.Left.height()
	rh := n.Right.height()

	if lh > rh {
		n.Height = lh + 1

		return
	}

	n.Height = rh + 1

	return
}

func (n *Node) balance() *Node {
	n.fixHeight()

	if n.bFactor() == 2 {
		if n.Right.bFactor() < 0 {
			n.Right = n.Right.rotateRight()
		}

		return n.rotateLeft()
	}

	if n.bFactor() == -2 {
		if n.Left.bFactor() > 0 {
			n.Left = n.Left.rotateLeft()
		}

		return n.rotateRight()
	}

	return n
}

func (n *Node) rotateRight() *Node {
	l := n.Left

	n.Left = l.Right
	if n.Left != nil {
		n.Left.Parent = n
	}

	l.Right = n
	n.Parent = l

	l.fixHeight()
	n.fixHeight()

	return l
}

func (n *Node) rotateLeft() *Node {
	r := n.Right

	n.Right = r.Left
	if n.Right != nil {
		n.Right.Parent = n
	}

	r.Left = n
	n.Parent = r

	r.fixHeight()
	n.fixHeight()

	return r
}

func (n *Node) WriteTo(t *tree.Tree) *tree.Tree {
	if n == nil {
		return t
	}

	t = t.AddChild(tree.NodeInt64(n.Key))

	if n.Left != nil {
		n.Left.WriteTo(t)
	}

	if n.Right != nil {
		n.Right.WriteTo(t)
	}

	return t
}

func (n *Node) totalHeight() uint8 {
	if n == n.Parent {
		return n.Height
	}

	return n.Parent.totalHeight()
}

func NewNode(key int, val interface{}) *Node {
	return &Node{Height: 1, Key: key, Value: val}
}
