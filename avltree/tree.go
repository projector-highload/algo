package avltree

import (
	"io"

	"github.com/m1gwings/treedrawer/tree"
)

type Tree struct {
	Root *Node
}

func (t *Tree) Insert(key int, val interface{}) {
	if t.Root == nil {
		t.Root = NewNode(key, val)
		return
	}

	t.Root = t.Root.insert(NewNode(key, val))
}

func (t *Tree) Remove(key int) {
	if t.Root == nil {
		return
	}

	t.Root = t.Root.remove(key).balance()
}

func (t *Tree) Find(key int) (interface{}, bool) {
	if t.Root == nil {
		return nil, false
	}

	if node := t.Root.find(key); node != nil {
		return node.Value, true
	}

	return nil, false
}

func (t *Tree) Print(w io.Writer) {
	renderTree := tree.NewTree(tree.NodeInt64(0))

	renderTree = t.Root.WriteTo(renderTree)

	io.WriteString(w, renderTree.String())
}

func New() *Tree {
	return &Tree{
		Root: nil,
	}
}
