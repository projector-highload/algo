package main

import (
	"log"

	"gitlab.com/projector-highload/algo/countingsort"
)

func main() {
	arr := []int{1, 56, 7, 2, 4, 8, 3, 9, 2312, 31, 543}

	countingsort.Sort(arr)

	log.Println(arr)
}
