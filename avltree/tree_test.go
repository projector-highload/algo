package avltree

import (
	"math/rand"
	"testing"
)

var treeSizes = map[string]int{
	"tree size 10 000":      10_000,
	"tree size 100 000":     100_000,
	"tree size 1 000 000":   1_000_000,
	"tree size 100 000 000": 100_000_000,
}

func benchmarkFind(b *testing.B, size int) {
	tree := makeTree(size)

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		k := rand.Intn(1_000_000)

		b.StartTimer()
		tree.Find(k)
		b.StopTimer()
	}
}

func BenchmarkTree_Find(b *testing.B) {
	for name, size := range treeSizes {
		size := size

		b.Run(name, func(b *testing.B) {
			benchmarkFind(b, size)
		})
	}
}

func benchmarkInsert(b *testing.B, size int) {
	tree := makeTree(size)

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		k := rand.Intn(1_000_000)

		b.StartTimer()
		tree.Insert(k, "")
		b.StopTimer()
	}
}

func BenchmarkTree_Insert(b *testing.B) {
	for name, size := range treeSizes {
		size := size

		b.Run(name, func(b *testing.B) {
			benchmarkInsert(b, size)
		})
	}
}

func benchmarkRemove(b *testing.B, size int) {
	tree := makeTree(size)

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		k := rand.Intn(1_000_000)

		b.StartTimer()
		tree.Remove(k)
		b.StopTimer()
	}
}

func BenchmarkTree_Remove(b *testing.B) {
	for name, size := range treeSizes {
		size := size

		b.Run(name, func(b *testing.B) {
			benchmarkRemove(b, size)
		})
	}
}

func makeTree(size int) *Tree {
	tree := New()

	for i := 0; i < size; i++ {
		tree.Insert(rand.Intn(1_000_000), "")
	}

	return tree
}
