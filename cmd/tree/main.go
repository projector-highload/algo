package main

import (
	"os"

	"gitlab.com/projector-highload/algo/avltree"
)

func main() {
	t := avltree.New()

	t.Insert(10, "tree 10")
	t.Insert(9, "tree 9")
	t.Insert(12, "tree 12")
	t.Insert(3, "tree 3")
	t.Insert(24, "tree 24")
	t.Insert(8, "tree 8")
	t.Insert(1000, "tree 1000")

	t.Print(os.Stdout)

	t.Remove(24)

	t.Print(os.Stdout)
}
