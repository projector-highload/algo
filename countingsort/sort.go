package countingsort

func Sort(arr []int) {
	if len(arr) == 0 {
		return
	}

	max := 0
	for _, i := range arr {
		if i > max {
			max = i
		}
	}

	counts := make([]int, max+1)

	for _, i := range arr {
		counts[i]++
	}

	pos := 0
	for val, count := range counts {
		for i := 0; i < count; i++ {
			arr[pos] = val
			pos++
		}
	}

	return
}
