package countingsort

import (
	"math/rand"
	"testing"
)

func BenchmarkSort(b *testing.B) {
	b.Run("array of 100 000 random int with max value 1 000 000", func(b *testing.B) {
		b.ResetTimer()

		for i := 0; i < b.N; i++ {
			b.StopTimer()
			r := rand.New(rand.NewSource(1))

			arr := make([]int, 100_000)

			for i := 0; i < 10_000; i++ {
				arr[i] = r.Intn(1_000_000)
			}

			b.StartTimer()
			Sort(arr)
		}
	})

	b.Run("array of 100 000 random int with max value 1 000", func(b *testing.B) {
		b.ResetTimer()

		for i := 0; i < b.N; i++ {
			b.StopTimer()
			r := rand.New(rand.NewSource(1))

			arr := make([]int, 100_000)

			for i := 0; i < 10_000; i++ {
				arr[i] = r.Intn(1_000)
			}

			b.StartTimer()
			Sort(arr)
		}
	})
}
